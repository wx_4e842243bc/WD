# 连享会学员交流问答区

#### 介绍
WD：问答的中文简称。记录连享会各个学员群的常见问题和解答。


&emsp;

##  &#x1F449;  [- 点此查看 WD -](https://gitee.com/arlionn/WD/wikis/Home)

&emsp;


> &#x1F353; 温馨提示：大家可以点击页面右上角【Fork】按钮，把本项目复制到你的 [码云](https://gitee.com/) 主页下，并定期同步更新。这样，你在「自己的地盘上」就可以随时翻阅了。 


&emsp;

&emsp;

&emsp;

&emsp;

&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> **线上直播 9 天**：2020.7.28-8.7  
> **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报750.png "连享会：Stata暑期班，9天直播")

&emsp; 



&emsp; 
> Stata连享会 &ensp;   [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0612/102507_695453ce_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。
